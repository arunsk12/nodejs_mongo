module.exports = function(app){
    var contacts = require('./controllers/contacts');

	app.get('/import', contacts.import);
    
	app.get('/contacts', contacts.findAll);
    app.get('/contacts/:id', contacts.findById);
    app.post('/contacts', contacts.add);
    app.put('/contacts/:id', contacts.update);
    app.delete('/contacts/:id', contacts.delete);
}