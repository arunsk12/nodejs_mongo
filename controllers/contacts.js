var mongoose = require('mongoose'),
Contact = mongoose.model('Contact');

exports.findAll = function(req, res){
  Contact.find({},function(err, results) {
    return res.send(results);
  });
};
exports.findById = function(req, res){
  var id = req.params.id;
  Contact.findOne({'_id':id},function(err, result) {
    return res.send(result);
  });
};
exports.add = function(req, res) {
  Contact.create(req.body, function (err, Contact) {
    if (err) return console.log(err);
    return res.send(Contact);
  });
}
exports.update = function(req, res) {
  var id = req.params.id;
  var updates = req.body;

  Contact.update({"_id":id}, req.body,
    function (err, numberAffected) {
      if (err) return console.log(err);
      console.log('Updated %d Contacts', numberAffected);
      res.send(202);
  });
}
exports.delete = function(req, res){
  var id = req.params.id;
  Contact.remove({'_id':id},function(result) {
    return res.send(result);
  });
};

exports.import = function(req, res){
  Contact.create(
    { "type_of_enquiry ": "General Enquiry", "title": "Mr", "first_name": "Test1", "last_name": "Test1", "reg_num" : "ABC0123", "dob": "11/11/1111", "email" : "test1@test.co.uk", "contact_number" : "01234567890", "postcode" : "SW15GH" },
    { "type_of_enquiry ": "General Enquiry", "title": "Mr", "first_name": "Test2", "last_name": "Test2", "reg_num" : "ABC0123", "dob": "11/11/2222", "email" : "test2@test.co.uk", "contact_number" : "01234567890", "postcode" : "SW15GH" },
    { "type_of_enquiry ": "General Enquiry", "title": "Mr", "first_name": "Test3", "last_name": "Test3", "reg_num" : "ABC0123", "dob": "11/11/3333", "email" : "test3@test.co.uk", "contact_number" : "01234567890", "postcode" : "SW15GH" },
    { "type_of_enquiry ": "General Enquiry", "title": "Mr", "first_name": "Test4", "last_name": "Test4", "reg_num" : "ABC0123", "dob": "11/11/4444", "email" : "test4@test.co.uk", "contact_number" : "01234567890", "postcode" : "SW15GH" }
  , function (err) {
    if (err) return console.log(err);
    return res.send(202);
  });
};