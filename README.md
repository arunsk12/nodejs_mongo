Create a REST API With Node.js and MongoDB
------------------------------------------

* Node.js - HTTP Server

* JavaScript modules - Controllers for the app that deals with data.

* Express - A minimal and flexible Node.js web application framework that provides robust set of features for web and mobile applications.

* Routes - URL Mapping (Specify HTTP Action, URL path and handler methods.

* Mongoose.js - Mongo Driver which interfaces between MongoDB and Node JS.

* Mongo - NO-SQL database.

* Npm - A package manager for node.

* PowerShell or any other ssh client.

Step-by-Step Implementation
---------------------------

1) Clone / Fork the project from the Bitbuket Git repository 
https://arunsk12@bitbucket.org/arunsk12/nodejs_mongo.git

2) Install npm 
npm install

3) Install mongoose driver
npm install mongoose --save-dev

4) Make sure you start the Mongo Daemon (e.g. mongod --dbpath=C:\Users\arun.kumar\Documents\NodeJS_Mongo\dbpath)

5) Run the server js file
node .\server.js

URLs to USE
------------
1) Import the data

http://localhost:3001/import (output will be "Accepted")

2) Lists all the contacts (FindAll)

http://localhost:3001/contacts

3) Find specific contact by ID (FindByID)

http://localhost:3001/contacts/XXXXXXXXXXXXXXXXXXX (replace XX with one of the _id from the step 2 output)

4) Curl command to UPDATE using the PUT verb. 

curl -i -X PUT -H 'Content-Type: application/json' -d '{"_id": "54b65691516682bc1124a3c0","title": "Mr","first_name": "updated_Test1","last_name": "updated_Test1","reg_num":"ABC0123","dob": "11/11/1111","email": "test1@test.co.uk","contact_number": "01234567890","postcode": "SW15GH"}' http://localhost:3001/contacts/54b65691516682bc1124a3c0

5) Curl command to ADD using the POST verb. 

curl -i -X POST -H 'Content-Type: application/json' -d '{"_id": "54b65691516682bc1124a9jj","title": "Mr","first_name": "Test5","last_name": "Test5","reg_num":"ABC0123","dob": "11/11/1111","email": "test1@test.co.uk","contact_number": "01234567890","postcode": "SW15GH"}' http://localhost:3001/contacts

6) Curl command to DELETE a particular contact. 

curl -i -X DELETE http://localhost:3001/contacts/54b65691516682bc1124a3c0

