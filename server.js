/*
* Server.js file to include required libraries, configuration etc 
*/

// Requires Express framework to  build for web with Node.js
var express = require('express'),
// Interfaces with MongoDB data 
mongoose = require('mongoose'),
fs = require('fs');

var mongoUri = 'mongodb://localhost/contact';
mongoose.connect(mongoUri);
var db = mongoose.connection;
db.on('error', function () {
  throw new Error('unable to connect to database at ' + mongoUri);
});

var app = express();

app.configure(function(){
  app.use(express.bodyParser());
});

require('./models/contact');
require('./routes')(app);

app.listen(3001);
console.log('Listening on port 3001...');