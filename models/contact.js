var mongoose = require('mongoose'),
Schema = mongoose.Schema;

var ContactSchema = new Schema({
  type_of_enquiry: String,
  title: String,
  first_name: String,
  last_name: String,
  reg_num: String,
  dob: String,
  email: String,
  contact_number: String,
  postcode: String
});

mongoose.model('Contact', ContactSchema);